![Logo](https://bitbucket.org/repo/Xxda4j/images/448304716-slhStartup.png)
===================
**Helio is a instant messenger.** It aims to be simple, quick and user-intuitive. At the moment there is only one client (for [windows](https://bitbucket.org/helio-im/windows-client)) but in the future it would be beneficial to create clients to multiple more platforms. The back-end is programmed in [Python](https://www.python.org/) and serves the clients using [Json](http://www.json.org/)

### Repo List:
 - [Server](https://gitlab.com/helio-im/server)
 - [Windows client](https://gitlab.com/helio-im/windows-client)
 - [Mobile](https://gitlab.com/helio-im/mobile)